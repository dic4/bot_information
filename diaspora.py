import diaspy
import json
from os import path
from infoKey import diaspora

channel = []

async def save_diaspora_config():
    file_config = []
    for element in channel:
        file_config.append([element.id, element.name])
    file = open("diaspora.json", "w")
    json.dump(file_config, file)
    file.flush()
    file.close()
    return

async def load_diaspora_config(client):
    list_read = []
    if path.exists("diaspora.json"):
        file = open("diaspora.json", "r")
        list_file = json.load(file)
        for element in list_file:
            list_read.append(client.get_channel(element[0]))
        file.close()
    return list_read

async def diaspora_send(message):
    if message.channel in channel:
        connection = diaspy.connection.Connection(pod=diaspora[0], username=diaspora[1], password=diaspora[2])
        connection.login()
        stream = diaspy.streams.Stream(connection)
        stream.post(text=message.content)
        connection.logout()

async def diaspora_set_command(message, info):
    global channel
    if str(message.guild.id) == str(diaspora[3]):
        channel = message.channel_mentions
    await save_diaspora_config()
    await message.channel.send("Cannaux liés a Diaspora {} {}".format(diaspora[1], channel))
    return len(message.channel_mentions) != 0

async def diaspora_del_command(message, info):
    global channel
    if str(message.guild.id) == str(diaspora[3]):
        channel = []
    await save_diaspora_config()
    await message.channel.send("Cannaux liés a Diaspora {} {}".format(diaspora[1], channel))
    return True

async def diaspora_show_command(message, info):
    await message.channel.send("Cannaux liés a Diaspora {} {}".format(diaspora[1], channel))
    return True

info = ["diaspora", "Relie un salon à une page diaspora"]

commands = [
    ["°diaspora_set",  True, diaspora_set_command,  "°diaspora_set [channel] : Règle les cannaux a rediriger vers diaspora"],
    ["°diaspora_del",  True, diaspora_del_command,  "°diaspora_del : Supprime la règle les cannaux a rediriger vers diaspora"],
    ["°diaspora_show", True, diaspora_show_command, "°diaspora_show : Affiche les cannaux a rediriger vers diaspora"]
]

async def start(client):
    global channel
    channel = await load_diaspora_config(client)
    return

async def exec():
    return

delai = 0
