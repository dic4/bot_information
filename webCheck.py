import json
from os import path
import requests

config = []
url_headers = {"Accept": 'application/vnd.github.full+json"text/html',
               'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36'}

async def save_config():
    file_config = []
    for element in config:
        file_config.append([element[0], element[1].id, element[1].name, [channel.id for channel in element[2]], [channel.name for channel in element[2]]])
    file = open("webcheck.json", "w")
    json.dump(file_config, file)
    file.flush()
    file.close()
    return

async def load_config(client):
    list_read = []
    if path.exists("webcheck.json"):
        file = open("webcheck.json", "r")
        list_file = json.load(file)
        for element in list_file:
            list_read.append([element[0], client.get_guild(element[1]), [client.get_channel(id) for id in element[3]]])
        file.close()
    return list_read

async def web_file_name(url):
    file_name = url
    file_name = file_name.replace("http://", "")
    file_name = file_name.replace("https://", "")
    file_name = file_name.replace("/", "_")
    file_name = file_name.replace(".", "_")
    file_name = file_name.replace("-", "_")
    file_name = file_name.replace("?", "_")
    file_name += ".html"
    return file_name

async def web_check():
    for c in config:
        curl = requests.get(c[0], headers=url_headers)
        current = curl.text#.encode("utf-8")
        file_name = await web_file_name(c[0])
        file_content = ""
        if path.exists(file_name):
            file = open(file_name, "r", encoding="utf-8", errors="ignore")
            file_content = file.read()#.encode("utf-8")
            file.close()
        if file_content != current.encode("utf-8"):
            for i in range(len(current)):
                if file_content[i] != current[i]:
                    print(i,file_content[i-20:i+20], current[i-20:i+20])
                    break
            if len(file_content) == 0:
                file_content = current
            else:
                for channel in c[2]:
                    await channel.send("La page web {} a été modifiée".format(c[0]))
            file_out = open(file_name, "w", encoding="utf-8", errors="ignore")
            file_out.write(file_content)
            file_out.flush()
            file_out.close()

async def web_set_command(message, info):
    global config
    found = False
    splitted = info.split()
    url = splitted[0]
    channel = message.channel_mentions
    for c in config:
        if c[0] == url:
            c[1] = message.guild
            c[2] = channel
            found = True
            break
    if not found:
        config.append([url, message.guild, channel])
    await save_config()
    await message.channel.send(config)
    return len(message.channel_mentions) != 0

async def web_del_command(message, info):
    global config
    found = False
    splitted = info.split()
    url = splitted[0]
    for c in range(len(config)):
        if config[c][0] == url:
            del config[c]
            found = True
            break
    await save_config()
    await message.channel.send(config)
    return found

async def web_show_command(message, info):
    await message.channel.send(config)
    return True

info = ["web", "Vérifie si une page web a changé et en informe dans un salon"]

commands = [
    ["°web_set",  True, web_set_command,  "°web_set [url] [channel] : Règle une page web a tester toutes les heures pour envoyer une alerte sur un canal quand la page a changé"],
    ["°web_del",  True, web_del_command,  "°web_del [url] : Supprime une règle de page web a tester"],
    ["°web_show", True, web_show_command, "°web_show : Affiche les pages web a tester et les cannaux de retour"]
]

async def start(client):
    global config
    config = await load_config(client)
    return

async def exec():
    await web_check()
    return

delai = 3600#10
