import json
import discord
from os import path

config = []

async def save_memory_config():
    file = open("memory.json", "w")
    json.dump(config, file)
    file.flush()
    file.close()
    return

async def load_memory_config(client):
    list_read = []
    if path.exists("memory.json"):
        file = open("memory.json", "r")
        list_read = json.load(file)
        file.close()
    return list_read

async def list(guild_id):
    global config
    elements = []
    for item, id, name, info in config:
        if id == guild_id:
            elements.append(item)
    return elements

async def set_command(message, info):
    global config
    result = False
    if len(info.split()) >= 2:
        found = False
        item = info.split()[0]
        info = info.replace(item, '', 1)[1:]
        for element in config:
            if element[0] == item and element[1] == message.guild.id:
                element[3] = info
                found = True
                break
        if not found:
            config.append([item, message.guild.id, message.guild.name, info])
        await save_memory_config()
        result = True
    return result

async def get_command(message, info):
    found = False
    result = False
    if info is not "":
        item = info.split()[0]
        for element in config:
            if element[0] == item and element[1] == message.guild.id:
                await message.channel.send(element[3])
                found = True
                break
        if not found:
            await message.channel.send("Liste des éléments en mémoire {}".format(await list(message.guild.id)))
        result = True
    return result

async def del_command(message, info):
    global config
    result = False
    if info is not "":
        item = info.split()[0]
        for i in range(len(config)):
            if config[i][0] == item and config[i][1] == message.guild.id:
                del config[i]
                break
        await save_memory_config()
        result = True
    return result

async def list_command(message, info):
    elements = []
    for item, id, name, info in config:
        if id == message.guild.id:
            elements.append(item)
    await message.channel.send("Liste des éléments en mémoire {}".format(await list(message.guild.id)))
    return True

async def get3p_command(message, info):
    await message.channel.send("", file=discord.File('3p.png'))
    return True

info = ["memoire", "Garde en mémoire des informations pour les restituer au besoin"]

commands = [
    ["°3p",   True,  get3p_command, "°3p : Affiche les Principes de Prises de Paroles"],
    ["°set",  True,  set_command,   "°set [item] [definition] : Ajoute une mémoire"],
    ["°get",  False, get_command,   "°get [item] : Récupère une mémoire"],
    ["°del",  False, del_command,   "°del [item] : Supprime une mémoire"],
    ["°list", True,  list_command,  "°list : Affiche la liste des elements en memoire"]
]

async def start(client):
    global config
    config = await load_memory_config(client)
    return

async def exec():
    return

delai = 0
