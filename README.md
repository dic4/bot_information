# Bot information
Bot de fonctionnement d'un groupe Discord, adapté au groupe DIC.

## Installation
Nécessite Python 3.7 avec les modules discord, diaspy, google-api, google-auth.

Un fichier "infoKey.py" est à ajouter sous la forme suivante :
```python
key = "clé du bot Discord"
diaspora = ["site Diaspora", "utilisateur", "mot de passe", "serveur Discord autorisé"]
youtube = ["clé API Youtube", ["serveur Discord autorisé"]]
```

## Modules
Voici les modules actuels :
- Module mp - Envoi des messages privé aux membres du serveur
- Module diaspora - Relie un salon à une page diaspora
- Module sauvegarde - Sauvegarde le contenu du serveur Discord
- Module liste_blanche - Autorise des personnes aux commandes avancées
- Module memoire - Garde en mémoire des informations pour les restituer au besoin
- Module yt - Recupère les commentaires d'une vidéo live youtube

Cette liste est donnée par la commande `°help`. La commande `°help [module]` donne les détails du module souhaité.

## Pour plus d'infos
Ce bot est développé par le groupe DIC sous le projet GitLab https://gitlab.com/dic4/bot_information

Plus d'informations vou sont données sur la page Wiki https://gitlab.com/dic4/bot_information/-/wikis/home
