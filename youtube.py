from infoKey import youtube
import googleapiclient.discovery

live_id = ""
last = []
yt_client = None
channel = None

async def get_live_id(id):
    result = ""
    request = yt_client.videos().list(
        part="liveStreamingDetails",
        id=id
    )
    response = request.execute()
    item = response["items"][0]
    if "liveStreamingDetails" in item.keys():
        if "activeLiveChatId" in item["liveStreamingDetails"].keys():
            result = item["liveStreamingDetails"]["activeLiveChatId"]
    return result

async def get_comments():
    global last
    if live_id != "":
        message = ""
        request = yt_client.liveChatMessages().list(
            liveChatId=live_id,
            part="snippet,authorDetails"
            #,maxResults=99
        )
        response = request.execute()
        for item in response["items"]:
            id = item["id"]
            snippet = item["snippet"]
            author_details = item["authorDetails"]
            if id not in last:
                last.append(id)
                ajout = "{} (YouTube) : {} [{}]\n".format(author_details["displayName"], snippet["displayMessage"], snippet["publishedAt"])
                if len(message) + len(ajout) >= 2000:
                    await channel.send(message[:-1])
                    message = ""
                message = message + ajout
            continue
        if message != "":
            await channel.send(message[:-1])

async def yt_start_command(message, info):
    global live_id
    global yt_client
    global channel
    result = False
    info = info.replace("https://www.youtube.com/watch?v=", '')
    if str(message.guild.id) in str(youtube[1]) and info is not "":
        if yt_client is None:
            yt_client = googleapiclient.discovery.build("youtube", "v3", developerKey=youtube[0])
            live_id = await get_live_id(info)
        if live_id == "":
            yt_client = None
            await message.channel.send("Le lien n'est pas un live Youtube.")
        else:
            last.clear()
            channel = message.channel
            await message.channel.send("Je me charge de retransmettre les commentaires de cette vidéo Youtube.")
        result = True
    return result

async def yt_stop_command(message, info):
    global live_id
    global yt_client
    if str(message.guild.id) in str(youtube[1]):
        live_id = ""
        last.clear()
        yt_client = None
        await message.channel.send("A votre service.")
    return True

async def yt_show_command(message, info):
    if live_id == "":
        await message.channel.send("Je suis en attente")
    else:
        await message.channel.send("Je relie la vidéo {} sur {}".format(live_id, channel.name))
    return True

info = ["yt", "Recupère les commentaires d'une vidéo live youtube"]

commands = [
    ["°yt_start", True, yt_start_command, "°yt_start [id] : Créer le lien pour récupérer les commentaires de la vidéo"],
    ["°yt_stop",  True, yt_stop_command,  "°yt_stop : Supprime l'association"],
    ["°yt_show",  True, yt_show_command,  "°yt_show : Affiche l'association"]
]

async def start(client):
    return

async def exec():
    await get_comments()
    return

delai = 60
