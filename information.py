import discord
import logging
import asyncio

from infoKey import key
#un fichier infoKey.py doit etre creee et contenir cette ligne : key = "...cle du bot..."

import liste_blanche
import mp
import diaspora
#import webCheck
import memory
import youtube
import sauvegarde
import listen

logger = logging.getLogger('discord')
logger.setLevel(logging.INFO)
handler = logging.FileHandler(filename='bot_information.log', encoding='utf-8', mode='w')
handler.setFormatter(logging.Formatter('%(asctime)s:%(levelname)s:%(name)s: %(message)s'))
logger.addHandler(handler)

client = discord.Client()
compteur = []

modules = [mp, diaspora, sauvegarde, liste_blanche, memory, youtube, listen]#, ]webCheck

async def run_ordonnanceur():
    while True:
        for i in range(len(modules)):
            if modules[i].delai != 0:
                compteur[i] = compteur[i] + 1
                if compteur[i] >= modules[i].delai:
                    await modules[i].exec()
                    compteur[i] = 0
        await asyncio.sleep(1)

@client.event
async def on_ready():
    for module in modules:
        compteur.append(0)
        await module.start(client)
    await client.change_presence(activity=discord.CustomActivity("DIC website"))
    print('We have logged in as {0.user}'.format(client))
    await client.loop.create_task(run_ordonnanceur())

@client.event
async def on_message(message: discord.Message):
    if message.author == client.user or message.content == "":
        return

    current_command = message.content.split()[0]
    for module in modules:
        for command in module.commands:
            if current_command == command[0]:
                if not command[1] or await liste_blanche.test_user_permission(message.guild, message.author, message.channel):
                    info = message.content.replace(command[0], '')
                    result = await command[2](message, info[1:])
                    if not result:
                        await message.channel.send(command[3])

    if message.content.startswith('°help'):
        answer = ""
        if len(message.content.split()) >= 2:
            for module in modules:
                if module.info[0] == message.content.split()[1]:
                    answer = answer + "**--- Module " + module.info[0] + " - " + module.info[1] + " ---**\n"
                    for command in module.commands:
                        if not command[1] or await liste_blanche.test_user_permission(message.guild, message.author, message.channel, silent=True):
                            answer = answer + command[3] + "\n"
        if answer == "":
            for module in modules:
                user_commands = False
                for command in module.commands:
                    if not command[1]:
                        user_commands = True
                        break
                if user_commands or await liste_blanche.test_user_permission(message.guild, message.author, message.channel):
                    answer = answer + "Module " + module.info[0] + " - " + module.info[1] + "\n"
        if not await liste_blanche.test_user_permission(message.guild, message.author, message.channel, silent=True):
            answer = answer + "Vous devez avoir plus de permissions pour les autres commandes"
        await message.channel.send(answer)

    if message.content.startswith('°invite'):
        url = discord.utils.oauth_url(client.user.id)
        await message.channel.send("Pour inviter ce bot sur votre serveur : <{0}>".format(url))

    if await liste_blanche.test_user_permission(message.guild, message.author, message.channel, silent=True):
        await diaspora.diaspora_send(message)

loop = asyncio.get_event_loop()
client.run(key, bot=True)
