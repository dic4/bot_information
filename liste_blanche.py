from os import path
import json

white_list = []

async def test_white_list(server, item_type, value):
    result = False
    for i in white_list:
        if i[0] == server.id and i[1] == item_type and i[2] == value.id:
            result = True
            break
    return result

async def save_white_wist():
    file = open("whiteListInformation.json", "w")
    json.dump(white_list, file)
    file.flush()
    file.close()
    return

async def add_white_list(server, item_type, value):
    if not await test_white_list(server, item_type, value):
        white_list.append((server.id, item_type, value.id, server.name, value.name))
        await save_white_wist()
    return

async def delete_white_list(server, item_type, value):
    for i in range(len(white_list)):
        if white_list[i][0] == server.id and white_list[i][1] == item_type and white_list[i][2] == value.id:
            del white_list[i]
            await save_white_wist()
            break
    return

async def load_white_list():
    list_read = []
    if path.exists("whiteListInformation.json"):
        file = open("whiteListInformation.json", "r")
        list_read = json.load(file)
        file.close()
    return list_read

async def test_user_permission(server, user, channel, silent = False):
    result = True #False
    if user is not None and hasattr(user, 'roles'):
        for role in user.roles:
            if role.permissions.manage_roles or role.permissions.manage_guild or role.permissions.administrator or await test_white_list(server, 'role', role):
                result = True
                break
    if not result:
        if await test_white_list(server, 'channel', channel):
            result = True
    if not result and not silent:
        await channel.send(
            "_Tout ce qui augmente la liberté augmente la responsabilité._ (Victor Hugo)\n\n"
            "_Tu dois avoir l'autorisation pour effectuer cette action :_\n"
            "- Soit en ayant le role **gerer les roles** dans ton role le plus fort\n"
            "- Soit en publiant dans un salon autorise en liste blanche\n"
            "- Soit en faisant parti d'un role autorise en liste blanche")
    return result

async def show_white_list(server, channel):
    roles = []
    channels = []
    for i in white_list:
        if i[0] == server.id:
            if i[1] == 'role':
                roles.append(i[4])
            if i[1] == 'channel':
                channels.append(i[4])
    await channel.send("Roles : {0}\nSalons : {1}".format(roles, channels))

async def add_role_command(message, info):
    result = False
    if len(message.role_mentions) > 0:
        result = True
        await add_white_list(message.guild, 'role', message.role_mentions[0])
    return result

async def del_role_command(message, info):
    result = False
    if len(message.role_mentions) > 0:
        result = True
        await delete_white_list(message.guild, 'role', message.role_mentions[0])
    return result

async def add_channel_command(message, info):
    result = False
    if len(message.channel_mentions) > 0:
        result = True
        await add_white_list(message.guild, 'channel', message.channel_mentions[0])
    return result

async def del_channel_command(message, info):
    result = False
    if len(message.channel_mentions) > 0:
        result = True
        await delete_white_list(message.guild, 'channel', message.channel_mentions[0])
    return result

async def liste_blanche_command(message, info):
    await show_white_list(message.guild, message.channel)
    return True

info = ["liste_blanche", "Autorise des personnes aux commandes avancées"]

commands = [
    ["°addRole",       True, add_role_command,      "°addRole {role} : Ajoute le role mentionne en liste blanche"],
    ["°delRole",       True, del_role_command,      "°delRole {role} : Retire le role mentionne de la liste blanche"],
    ["°addChannel",    True, add_channel_command,   "°addChannel [channel] : Ajoute le salon mentionne (sinon le salon courant) en liste blanche"],
    ["°delChannel",    True, del_channel_command,   "°delChannel [channel] : Retire le salon mentionne (sinon le salon courant) de la liste blanche"],
    ["°liste_blanche", True, liste_blanche_command, "°liste_blanche : affiche la liste blanche"]
]

async def start(client):
    global white_list
    white_list = await load_white_list()
    return

async def exec():
    return

delai = 0
