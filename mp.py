import time
import asyncio
import liste_blanche
import discord
from asyncio import CancelledError

MP_PACKAGE_TIME = 600
MP_PACKAGE_NUMBER = 10

ordonnanceurTaches = []
ordonnaceurStatus = dict()
ordonnaceurStatus['temps'] = 0
ordonnaceurStatus['nombre'] = 0
ordonnaceurStatus['courant'] = 0

prochainIdentifiant = 1
client = discord.Client()

async def envoi(tache, membre):
    #try:
    if not tache['fake']:
        await membre.send(tache['msg'])
    tache['success'] += 1
    endTime = (int(((len(tache['members']) - ordonnaceurStatus['courant']) + ordonnaceurStatus['nombre'] - 1) / MP_PACKAGE_NUMBER) * MP_PACKAGE_TIME) + (len(tache['members']) - ordonnaceurStatus['courant']) + ordonnaceurStatus['nombre']
    await asyncio.sleep(1)
    if tache['res'] is not None:
        message = 'planting seeds, please wait... **{0}**/**{1}**'.format(tache['success'], len(tache['members']))
        if endTime > 60:
            message = message + " will finish at " + time.asctime(time.localtime(time.time() + endTime))
        await tache['res'].edit(content=message)
    #print('planting seeds, please wait... **{0}**/**{1}**'.format(tache['success'], len(tache['members'])))
    #except:
    #    print("Unexpected error:", sys.exc_info()[0])
    #    tache['fail'] += 1

async def reponseTermine(tache):
    percent = tache['success'] / len(tache['members']) * 100

    if tache['res'] is not None:
        await tache['res'].edit(content='_Sent to **{0}** users. (**{1}**%, **{2}** fail)_'.format(tache['success'],
                                                                                    round(percent, 2),
                                                                                    tache['fail']))
    #print('_Sent to **{0}** users. (**{1}**%, **{2}** fail)_'.format(tache['success'], round(percent, 2), tache['fail']))

async def testOrdonnanceur():
    result = False
    if time.time() - ordonnaceurStatus['temps'] > MP_PACKAGE_TIME:
        result = True
        ordonnaceurStatus['nombre'] = 0
    elif ordonnaceurStatus['nombre'] < MP_PACKAGE_NUMBER:
        result = True
    return result

async def runOrdonnanceurAsyncOnce():
    stop = False
    if await testOrdonnanceur():
        if len(ordonnanceurTaches) >= 1:
            print(ordonnanceurTaches)
            print(ordonnaceurStatus)
            for j in range(ordonnaceurStatus['courant'], len(ordonnanceurTaches[0]['members'])):
                #print("envoi", i, i['members'][j])
                await envoi(ordonnanceurTaches[0], ordonnanceurTaches[0]['members'][j])
                ordonnaceurStatus['temps'] = time.time()
                ordonnaceurStatus['nombre'] = ordonnaceurStatus['nombre'] + 1
                ordonnaceurStatus['courant'] = j+1
                if ordonnaceurStatus['nombre'] >= MP_PACKAGE_NUMBER:
                    stop = True
                    break
            if not stop or ordonnaceurStatus['courant'] >= len(ordonnanceurTaches[0]['members']):
                #print("termine", i)
                await reponseTermine(ordonnanceurTaches[0])
                del ordonnanceurTaches[0]
                ordonnaceurStatus['courant'] = 0

async def estimateTime(number):
    numberBefore = sum([len(i['members']) for i in ordonnanceurTaches]) - ordonnaceurStatus['courant']
    startTime = (int((numberBefore + ordonnaceurStatus['nombre'] - 1) / MP_PACKAGE_NUMBER) * MP_PACKAGE_TIME) + 2*numberBefore
    endTime = (int((numberBefore + number + ordonnaceurStatus['nombre'] - 1) / MP_PACKAGE_NUMBER) * MP_PACKAGE_TIME) + 2*numberBefore + 2*number
    return startTime, endTime

async def send_all(ctx, msg, members, only_connected=False, fake=False):
    if msg == "":
        await ctx.channel.send('_Empty messages can hardly be understood by ordinary people..._')
        return False

    if not await liste_blanche.test_user_permission(ctx.guild, ctx.author, ctx.channel):
        return False

    msg += '\n\n_Envoyé par le serveur_ **{0}**\n-------------------------------'.format(ctx.guild.name)

    startTime, endTime = await estimateTime(len(members))
    messageChannel = "planting seeds, please wait..."
    if startTime > 60:
        messageChannel = messageChannel + " will start at " + time.asctime(time.localtime(time.time()+startTime))
    if endTime > 60:
        messageChannel = messageChannel + " will finish at " + time.asctime(time.localtime(time.time()+endTime))
    try:
        res = await ctx.channel.send(messageChannel)
    except CancelledError:
        res = None

    tache = dict()
    global prochainIdentifiant
    tache['id'] = prochainIdentifiant
    tache['res'] = res
    tache['author'] = ctx.author
    tache['msg'] = msg
    if only_connected:
        tache['members'] = []
        for i in members:
            if i.status != discord.Status.offline and i != client.user:
                tache['members'].append(i)
    else:
        tache['members'] = []
        for i in members:
            if i != client.user:
                tache['members'].append(i)
    tache['fake'] = fake
    tache['success'] = 0
    tache['fail'] = 0
    ordonnanceurTaches.append(tache)
    prochainIdentifiant = (prochainIdentifiant + 1) % 1000

async def send_all_command(message, info):
    if len(message.role_mentions) > 0:
        info = discord.utils.escape_mentions(info)
        members = message.role_mentions[0].members
    else:
        members = message.guild.members
    await send_all(message, info, members)
    return True

async def send_co_command(message, info):
    if len(message.role_mentions) > 0:
        info = discord.utils.escape_mentions(info)
        members = message.role_mentions[0].members
    else:
        members = message.guild.members
    await send_all(message, info, members, only_connected=True)
    return True

async def send_fake_command(message, info):
    if len(message.role_mentions) > 0:
        info = discord.utils.escape_mentions(info)
        members = message.role_mentions[0].members
    else:
        members = message.guild.members
    await send_all(message, info, members, fake=True)
    return True

async def show_tasks_command(message, info):
    server = message.guild
    channel = message.channel
    msg = ""
    if len(ordonnanceurTaches) == 0:
        msg = "Au repos"
    for i in range(len(ordonnanceurTaches)):
        task = ordonnanceurTaches[i]
        msg = msg + "tâche n°{}, auteur \"{}\", serveur \"{}\", canal \"{}\", nombre de destinataires {}\n".format(task['id'], task['author'], task['res'].author.guild, task['res'].channel, len(task['members']))
    await channel.send(msg)
    return True

async def kill_task_command(message, info):
    result = False
    splitted = info.split()
    if len(splitted) != 0:
        result = True
        id = int(splitted[0])
        msg = "Id {} not found".format(id)
        for i in range(len(ordonnanceurTaches)):
            if ordonnanceurTaches[i]['id'] == id:
                if ordonnanceurTaches[i]['res'].author.guild == message.guild:
                    if i == 0:
                        ordonnaceurStatus['courant'] = len(ordonnanceurTaches[0]['members'])
                    else:
                        del ordonnanceurTaches[i]
                    msg = "Id {} killed".format(id)
                else:
                    msg = "Cannot kill on another server"
        await message.channel.send(msg)
    return result

info = ["mp", "Envoi des messages privé aux membres du serveur"]

commands = [
    ["°send_all",  True, send_all_command,   "°send_all [role] : Envoi de MP a tout les membres du serveur ou ceux du premier role mentionne"],
    ["°send_co",   True, send_co_command,    "°send_co [role] : Envoi de MP a tout les membres du serveur actuellement connectees ou ceux du premier role mentionne"],
    ["°send_fake", True, send_fake_command,  "°send_fake [role] : Simule l'envoi de MP a tout les membres du serveur ou ceux du premier role mentionne"],
    ["°tasks",     True, show_tasks_command, "°tasks : Affiche les ordres d'envois de MP en cours"],
    ["°kill",      True, kill_task_command,  "°kill [id] : Supprime un ordre d'envoi de MP en cours (par son identifiant)"]
]

async def start(client):
    return

async def exec():
    await runOrdonnanceurAsyncOnce()
    return

delai = 2
