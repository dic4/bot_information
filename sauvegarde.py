import json
import requests
from os import path

url_headers = {"Accept": 'application/vnd.github.full+json"text/html',
               'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36'}
data = []
member = None

async def get_attachment(channel, name, url):
    curl = requests.get(url, headers=url_headers)
    picture = open("dump/"+channel+"_"+name, "wb")
    picture.write(curl.content)
    picture.flush()
    picture.close()

async def save_file(name):
    file = open("dump/"+name+".json", "w")
    json.dump(data, file)
    file.flush()
    file.close()
    return

async def channel_saved(name):
    return path.exists("dump/"+name+".json")

async def save_guild(guild):
    global data
    channels = guild.text_channels
    for channel in channels:
        if channel.permissions_for(guild.me).read_messages:
            if not await channel_saved(channel.name):
                print(channel.name)
                data = []
                async for message in channel.history(limit=200):
                    element = [message.author.name, str(message.created_at), message.content]
                    #if "attachments" in message.keys():
                    for a in message.attachments:
                            element.append(a.filename)
                            await get_attachment(channel.name, a.filename, a.url)
                    data.append(element)
                await save_file(channel.name)
        else:
            print(channel.name + " non autorisé")
    return True

async def save_command(message, info):
    await save_guild(message.guild)
    return True

info = ["sauvegarde", "Sauvegarde le contenu du serveur Discord"]

commands = [
    ["°save",  True,  save_command,  "°save [mail] : Sauvegarde le contenu du serveur Discord"]
]

async def start(client):
    global member
    member = client.user
    return

async def exec():
    return

delai = 0
